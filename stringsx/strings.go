package stringsx

import (
	"sort"
	"unsafe"
)

// ToBytes 高效的将字符串转换成可编辑字节数组
func ToBytes(str string) []byte {
	x := (*[2]uintptr)(unsafe.Pointer(&str))
	h := [3]uintptr{x[0], x[1], x[1]}
	return *(*[]byte)(unsafe.Pointer(&h))
}

// InArray 字符串是否在数组中
func InArray(target string, list []string) bool {
	for _, element := range list {
		if element == target {
			return true
		}
	}
	return false
}

// InArraySorted 字符串是否在已排序的数组中
func InArraySorted(target string, list []string) bool {
	index := sort.SearchStrings(list, target)
	if index < len(list) && list[index] == target {
		return true
	}
	return false
}

// HashCode 计算字符串的哈希值
func HashCode(str string) int {
	var hash = 0
	for i := 0; i < len(str); i++ {
		hash = 31*hash + int(str[i])
	}
	return hash
}
