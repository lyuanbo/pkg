```shell
$ go test -bench='^\QBenchmarkToBytes\E$' -benchtime=5s -count=3
goos: darwin
goarch: amd64
pkg: pkg/x/strings
cpu: Intel(R) Core(TM) i3-8100B CPU @ 3.60GHz
BenchmarkToBytes-4      1000000000               0.2804 ns/op
BenchmarkToBytes-4      1000000000               0.2790 ns/op
BenchmarkToBytes-4      1000000000               0.2789 ns/op
PASS
ok      pkg/x/strings   1.022s
```

```shell
$ go test -bench='^\QBenchmarkBytes\E$' -benchtime=5s -count=3
goos: darwin
goarch: amd64
pkg: pkg/x/strings
cpu: Intel(R) Core(TM) i3-8100B CPU @ 3.60GHz
BenchmarkBytes-4        1000000000               5.075 ns/op
BenchmarkBytes-4        1000000000               5.057 ns/op
BenchmarkBytes-4        1000000000               5.056 ns/op
PASS
ok      pkg/x/strings   16.902s
```

```shell
$ go test -bench='^\QBenchmarkInArray\E$' -benchtime=5s -count=3
goos: darwin
goarch: amd64
pkg: gitee.com/lyuanbo/pkg/x/strings
cpu: Intel(R) Core(TM) i3-8100B CPU @ 3.60GHz
BenchmarkInArray-4            3974           1428637 ns/op
BenchmarkInArray-4            4284           1418190 ns/op
BenchmarkInArray-4            4244           1381499 ns/op
PASS
ok      gitee.com/lyuanbo/pkg/x/strings 24.825s
```

```shell
$ go test -bench='^\QBenchmarkInArraySorted\E$' -benchtime=5s -count=3
goos: darwin
goarch: amd64
pkg: gitee.com/lyuanbo/pkg/x/strings
cpu: Intel(R) Core(TM) i3-8100B CPU @ 3.60GHz
BenchmarkInArraySorted-4         8711146               693.1 ns/op
BenchmarkInArraySorted-4         8773696               686.3 ns/op
BenchmarkInArraySorted-4         8756209               682.1 ns/op
PASS
ok      gitee.com/lyuanbo/pkg/x/strings 26.674s
```