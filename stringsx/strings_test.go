package stringsx

import (
	"math/rand"
	"reflect"
	"sort"
	"testing"
	"time"

	"gitee.com/lyuanbo/pkg/bytesx"
)

func TestToBytes(t *testing.T) {
	str := "test_to_bytes"
	t.Run(str, func(t *testing.T) {
		if got := ToBytes(str); !reflect.DeepEqual(got, []byte(str)) {
			t.Errorf("ToBytes() = %v, want %v", got, []byte(str))
		}
	})
}

func BenchmarkToBytes(b *testing.B) {
	str := "test_to_bytes"
	for i := 0; i < b.N; i++ {
		ToBytes(str)
	}
}

func Bytes(str string) []byte {
	return []byte(str)
}
func BenchmarkBytes(b *testing.B) {
	str := "test_to_bytes"
	for i := 0; i < b.N; i++ {
		Bytes(str)
	}
}

func TestInArray(t *testing.T) {
	type args struct {
		target string
		list   []string
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "true",
			args: args{
				target: "test",
				list:   []string{"t", "z", "test", "w"},
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			sort.Strings(tt.args.list)
			if got := InArray(tt.args.target, tt.args.list); got != tt.want {
				t.Errorf("ContainsSlice() = %v, want %v", got, tt.want)
			}
		})
	}
}

var r = rand.New(rand.NewSource(time.Now().Unix()))

func randString() string {
	length := r.Intn(10)
	bytes := make([]byte, length)
	for i := 0; i < length; i++ {
		b := r.Intn(26) + 65
		bytes[i] = byte(b)
	}
	return bytesx.ToString(bytes)
}

func BenchmarkInArray(b *testing.B) {
	b.StopTimer()
	var arr []string
	for i := 0; i < 1000000; i++ {
		arr = append(arr, randString())
	}
	b.StartTimer()
	for i := 0; i < b.N; i++ {
		var str = randString()
		InArray(str, arr)
	}
}

func BenchmarkInArraySorted(b *testing.B) {
	b.StopTimer()
	var arr []string
	for i := 0; i < 1000000; i++ {
		arr = append(arr, randString())
	}
	sort.Strings(arr)
	b.StartTimer()
	for i := 0; i < b.N; i++ {
		var str = randString()
		InArraySorted(str, arr)
	}
}

func TestHashCode(t *testing.T) {
	type args struct {
		str string
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "abcd",
			args: args{
				str: "abcd",
			},
			want: 0,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := HashCode(tt.args.str); got != tt.want {
				t.Errorf("HashCode() = %v, want %v", got, tt.want)
			}
		})
	}
}
