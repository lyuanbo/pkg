package bytesx

import (
	"reflect"
	"testing"
)

var str = "hello world"
var buf = []byte(str)

func TestToString(t *testing.T) {
	t.Run(str, func(t *testing.T) {
		if got := ToString(buf); !reflect.DeepEqual(got, str) {
			t.Errorf("ToString() = %v, want %v", got, str)
		}
	})
}

func BenchmarkToString(b *testing.B) {
	for i := 0; i < b.N; i++ {
		ToString(buf)
	}
}

func String(buf []byte) string {
	return string(buf)
}
func BenchmarkString(b *testing.B) {
	for i := 0; i < b.N; i++ {
		String(buf)
	}
}
