package bytesx

import "unsafe"

// ToString 高效的将字节数组转字符串
func ToString(buf []byte) string {
	return *(*string)(unsafe.Pointer(&buf))
}
