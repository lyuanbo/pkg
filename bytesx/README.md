```shell
$ go test -bench='^\QBenchmarkToString\E$' -benchtime=5s  -count=3
goos: darwin
goarch: amd64
pkg: pkg/x/bytes
cpu: Intel(R) Core(TM) i3-8100B CPU @ 3.60GHz
BenchmarkToString-4     1000000000               0.2790 ns/op
BenchmarkToString-4     1000000000               0.2790 ns/op
BenchmarkToString-4     1000000000               0.2793 ns/op
PASS
ok      pkg/x/bytes     1.427s
```

```shell
$ go test -bench='^\QBenchmarkString\E$' -benchtime=5s  -count=3 
goos: darwin
goarch: amd64
pkg: pkg/x/bytes
cpu: Intel(R) Core(TM) i3-8100B CPU @ 3.60GHz
BenchmarkString-4       1000000000               3.923 ns/op
BenchmarkString-4       1000000000               3.918 ns/op
BenchmarkString-4       1000000000               3.915 ns/op
PASS
ok      pkg/x/bytes     13.063s
```