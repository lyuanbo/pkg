package pathx

import (
	"testing"
)

func TestGetExePath(t *testing.T) {
	tests := []struct {
		name    string
		want    string
		wantErr bool
	}{
		{
			name:    "test1",
			want:    "/Users/yidejia/Desktop/GoWork/pkg",
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := GetExePath()
			if (err != nil) != tt.wantErr {
				t.Errorf("GetExePath() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("GetExePath() got = %v, want %v", got, tt.want)
			}
		})
	}
}
